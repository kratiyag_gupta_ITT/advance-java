

/**
 * 
 * Invokes when SignUp button Pressed
 * 
 * It first validate existance of User
 *  if Exist then Show Error Message
 *  else Save user Data
 */
var users=new Array();
var newUser;
function storeData()
{
    user=JSON.parse(localStorage.getItem("users"));
    var fName = document.getElementById("first_name").value;
    var lName = document.getElementById("signup_lastname").value;
    var uName = document.getElementById("signup_username").value;
    if(user==null)
        {
            var password = document.getElementById("signup_password").value;
            newUser = new person(fName, lName, uName, password);
            addDataInArray(newUser);
            alert("Register Sucessfully");
            sessionStorage.setItem(uName, JSON.stringify(newUser));   //Data Storage for session
            //window.location.href = "welcomPage.html?uname=" +uName;
            return false;
        }
    else
        {
            for(x in user)
            {
                if(user[x].userName==uName)
                {   
                alert("User Id Already Exist Enter a New One");
                return false;
                }
            }
        }
    var password = document.getElementById("signup_password").value;
    if(password!=document.getElementById("confirmation_password").value)
        {
            alert("Password Not match");
            return false;
        }
    newUser = new person(fName, lName, uName, password);
    addDataInArray(newUser);
    alert("Register Sucessfully");
    sessionStorage.setItem(uName, JSON.stringify(newUser));
   // window.location.href = "welcomPage.html?uname=" +uName;
    return false;
}

/**
 * 
 * @param {*First Name of Person} fname 
 * @param {*Last Name of Person} lname 
 * @param {*Used Id} userName 
 * @param {*Store PAssword} password 
 * 
 * Creation of Person Object
 */

function person(fname, lname, userName, password)
{
    this.firstname = fname;
    this.lastname = lname;
    this.userName = userName;
    this.password = password;
    this.signUrl = "";
}

/**
 * 
 * Invokes when when user logins
 */

function loginUser()
{
    var uName = document.getElementById("login_username").value;
    var password = document.getElementById("login_password").value;
    user = JSON.parse(localStorage.getItem("users"));
    for(x in user)
    {
        if(user[x].userName== uName && user[x].password==password)
        {
            sessionStorage.setItem(uName, JSON.stringify(user[x]));    
            window.location.href = "welcomPage.html?uname=" + uName;
            return false;
        }
    }   
    alert("Invali UserName Or Password");
        return false;
   
}

/**
 * 
 * Add data in Array in Local Storage
 * 
 */

function addDataInArray(newUser)
{
    alert(newUser.userName);
    try
    {
        users.push(newUser);
    }
    catch(err)
    {
        alert("Unable to Save Data");
        return false;
    }
    localStorage.setItem("users",JSON.stringify(users));
    alert("data added");
}

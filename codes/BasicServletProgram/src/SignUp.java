

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginSignUpValidation
 */
@WebServlet("/SignUp")
public class SignUp extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		request.getSession().setAttribute("userId",request.getParameter("signup_username"));
		deatailesPrinter(request,response);
		ServletConfig config=getServletConfig();  
	    String driver=config.getInitParameter("driver");  
	    response.getWriter().print("\nDriver is: "+driver);  	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
		{
			doGet(request, response);
		}
	/**
	 * 
	 * It is used to print the details of user entered in in sign up form 
	 * 
	 */
	protected void deatailesPrinter(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 

		{
			PrintWriter out=response.getWriter();
			out.print("\n");
			out.print("<html>");
			out.print("<body>");
			out.print("<table>");
			out.print("<tr>");
			out.print("<th>");
			out.print("Pearson Name");
			out.print("</th>");
			out.print("<th>");
			out.print("User Name");
			out.print("</th>");
			out.print("<th>");
			out.print("Password");
			out.print("</th>");
			out.print("</tr>");
			out.print("<tr>");
			out.print("<td>");
			out.print(request.getParameter("first_name")+" "+request.getParameter("signup_lastname"));
			out.print("</td>");
			out.print("<td>");
			out.print(request.getParameter("signup_username"));
			out.print("</td>");
			out.print("<td>");
			out.print(request.getParameter("signup_password"));
			out.print("</td>");
			out.print("</tr>");
			out.print("</table>");
			out.print("Session is set to user Name :- ");
			out.println((String)request.getSession().getAttribute("userId"));
			out.print("</body>");
			out.print("</html>");
		}
}



import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginServelet
 */
@WebServlet("/LoginServelet")
public class LoginServelet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
		{
		
					if(request.getParameter("login_password").equals("kratiyag"))
						{  
							RequestDispatcher rd=request.getRequestDispatcher("WelcomePageServelet");  
							rd.forward(request, response);  
						}  
					else
						{  
					        response.getWriter().print("Sorry UserName or Password Error!");  
					        RequestDispatcher rd=request.getRequestDispatcher("/loginSignup.html");  
					        rd.include(request, response);  		                      
						}
		  }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
		{
			doGet(request, response);
		}

}

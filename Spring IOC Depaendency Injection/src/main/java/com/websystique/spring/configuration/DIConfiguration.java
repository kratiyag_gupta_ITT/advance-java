package com.websystique.spring.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import com.websystique.spring.domain.EmailService;
import com.websystique.spring.domain.MessageService;

@Configuration
@ComponentScan(value = { "com.websystique.spring.domain" })
public class DIConfiguration
	{

		/**
		 * creates bean
		 * @return		object of Email Services
		 */
		@Bean
		public MessageService getMessageService()
			{
				return new EmailService();
			}
		
	}
package com.springProject.BeanScope;

import java.util.Scanner;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.springProject.configuration.MessageConfiguration;
import com.springProject.domain.Greetings;
import com.springProject.domain.Message;
import com.springProject.domain.WelcomeMessage;

/**
 * Main Driver class class in Springs
 *
 */
public class App
	{
		public static void main(String[] args)
			{
				Scanner in = new Scanner(System.in);
				AnnotationConfigApplicationContext contex = new AnnotationConfigApplicationContext(
						MessageConfiguration.class);
				Message app = contex.getBean(Greetings.class);
				Message app1 = contex.getBean(Greetings.class);
				app.printMessage("Two Objects are Created ,below are the Reference Value");
				System.out.println(app);
				System.out.println(app1);
				if (app == app1)
					{
						System.out.println("This bean has singelton Scope\n");
					} else
					{
						System.out.println("This bean has prototype Scope\n");
					}
				Message app3 = contex.getBean(WelcomeMessage.class);
				Message app4 = contex.getBean(WelcomeMessage.class);
				app3.printMessage("Two Objects are Created ,below are the Reference Value");
				System.out.println(app3);
				System.out.println(app4);
				if (app3 == app4)
					{
						System.out.println("This bean has singelton Scope");
					} else
					{
						System.out.println("This bean has prototype Scope");
					}
				contex.close();
				in.close();
			}
	}

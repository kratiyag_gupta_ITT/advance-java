package com.springProject.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import com.springProject.domain.Greetings;
import com.springProject.domain.WelcomeMessage;

@Configuration
@ComponentScan(value = { "com.springProject.domain" })       //Looking for Classes in Specified Package
public class MessageConfiguration
	{
		@Bean
		@Scope("prototype")									//for determining the scope of Bean
		public Greetings getMessage()
			{
				return new Greetings();
			}

		@Bean												//If No Scope then the Default Scope in Singleton 			
		public WelcomeMessage getWelcomeMessage()
			{
				return new WelcomeMessage();
			}

	}

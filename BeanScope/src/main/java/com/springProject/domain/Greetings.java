package com.springProject.domain;

import java.time.LocalTime;

public class Greetings implements Message
	{
		String str;
		public void printMessage(String str)
			{
				
				if(LocalTime.now().getHour()>=12&&LocalTime.now().getHour()<16)
					{
						System.out.println("Good afternoon,\n\t"+str);
					}
				else if(LocalTime.now().getHour()>=16&&LocalTime.now().getHour()<20)
					{
						System.out.println("Good Evening,\n\t"+str);
					}
				else if(LocalTime.now().getHour()>=20&&LocalTime.now().getHour()<0)
					{
						System.out.println("Good Night,\n\t "+str);
					}
				else 
					{
						System.out.println("Good Morning,\n\t "+str);
					}
			}
		
	}

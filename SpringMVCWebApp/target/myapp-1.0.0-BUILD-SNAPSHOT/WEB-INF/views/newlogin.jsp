<%-- 
    Document   : newlogin
    Created on : Aug 13, 2017, 6:12:03 PM
    Author     : kratiyag.gupta
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login Page</title>
        <!-- Javascript -->
        <spring:url value="/resource/js/jquery-1.11.1.min.js.download" var="jquery" />
        <spring:url value="/resource/js/jquery.backstretch.min.js.download" var="backstretch" />
        <spring:url value="/resource/js/wow.min.js.download" var="wow" />
        <spring:url value="/resource/js/retina-1.1.0.min.js.download" var="retina" />
        <spring:url value="/resource/js/scripts.js.download" var="script" />
    <a href="/resource/js/bootstrap.min.js.download"></a>
    <script src="${jquery}"></script>
    <script src="${backstretch}"></script>
    <script src="${wow}"></script>
    <script src="${retina}"></script>
    <script src="${script}"></script>
    <spring:url value="/resource/css/style.css" var="registerCSS" />
    <spring:url value="/resource/css/bootstrap.min.css" var="bootstrap" />
    <spring:url value="/resource/css/css" var="css" />
    <spring:url value="/resource/css/form-elements.css" var="formCSS" />
    <spring:url value="/resource/css/media-queries.css" var="mediaQueries" />
    <spring:url value="/resource/css/typicons.min.css" var="iconCSS" />
    <spring:url value="/resource/images/1.jpg" var="background" />
    <link href="${registerCSS}" rel="stylesheet" type="text/css"/>
    <link href="${bootstrap}" rel="stylesheet" type="text/css"/>
    <link href="${css}" rel="stylesheet" type="text/css"/>
    <link href="${formCSS}" rel="stylesheet" type="text/css"/>
    <link href="${mediaQueries}" rel="stylesheet" type="text/css"/>
    <link href="${iconCSS}" rel="stylesheet" type="text/css"/>

    <!-- Favicon and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://azmind.com/premium/marco/v2-4/layout-1/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://azmind.com/premium/marco/v2-4/layout-1/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://azmind.com/premium/marco/v2-4/layout-1/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="http://azmind.com/premium/marco/v2-4/layout-1/assets/ico/apple-touch-icon-57-precomposed.png">
    <!--<jspinclude page="cssLoader.jsp"></jspinclude>-->
</head>

<body>

    <!-- Loader -->
    <div class="loader" style="display: none;">
        <div class="loader-img" style="display: none;"></div>
    </div>

    <!-- Top content -->
    <div class="top-content" style="position: relative; z-index: 0; background: none;">

        <!-- Top menu -->
        <nav class="navbar navbar-inverse navbar-no-bg" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">IntimeTec VisonSoft Pvt. Ltd. </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="top-navbar-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="scroll-link" href="#">Features</a></li>
                        <li><a class="scroll-link" href="#">How it works</a></li>
                        <li><a class="scroll-link" href="#">About</a></li>
                        <li><a class="scroll-link" href="#">Testimonials</a></li>
                        <li><a class="btn btn-link-2" href="newregister">Sign Up</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="inner-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-7 text">
                        <h1 class="wow fadeInLeftBig animated" style="visibility: visible; animation-name: fadeInLeftBig;"><strong>Learn and Grow Daily</strong></h1>
                        <div class="description wow fadeInLeftBig animated" style="visibility: visible; animation-name: fadeInLeftBig;">
                            <p class="banner-font-style">"HARD WORK BEATS TALENT WHEN TALENT DOSEN'T WORK HARD" </p>
                            <p>Working Hard for Something we don't want is called Stress.</p>
                            <p>Working Hard for Something we want is called Passion.</p>
                        </div>
                        <div class="top-big-link wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <a class="btn btn-link-1 scroll-link" href="newregister">Sign Up</a>
                            <a class="btn btn-link-2 scroll-link" href="#">Learn more</a>
                        </div>
                    </div>
                    <div class="col-sm-5 form-box wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Log In</h3>
                            </div>
                            <div class="form-top-right">
                                <span aria-hidden="true" class="glyphicon glyphicon-pencil"></span>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <form role="form" action="loginProcess" modelAttribute="login" method="post">
                                <div class="form-group">
                                    <label class="sr-onl" path="username">Enter Employee Email Id</label>
                                    <input type="number" path="username" valur="${login.username}" name="username" placeholder="Employee Email Id..." class="form-first-name form-control" id="username">
                                    <label class="warning">${usernameErrorMessage}</label>
                                </div>
                                <div class="form-group">
                                    <label class="sr-onl" path="lastName">Password</label>
                                    <input type="password" name="password" path="password" placeholder="*********" class="form-last-name form-control" id="password">
                                    <label class="warning">${passwordErrorMessage}</label>
                                </div>
                                <button type="submit" class="btn">Log In</button>
                                <div class="form-links">
                                    <a href="#" class="launch-modal" data-modal-id="modal-privacy">Privacy Policy</a> - 
                                    <a href="#" class="launch-modal" data-modal-id="modal-faq">FAQ</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="backstretch" style="left: 0px; top: 0px; overflow: hidden; margin: 0px; padding: 0px; height: 907px; width: 1349px; z-index: -999998; position: absolute;">
            <img src="${background}" style="position: absolute; margin: 0px; padding: 0px; border: none; width: 1349px; height: 1011.75px; max-height: none; max-width: none; z-index: -999999; left: 0px; top: -52.375px;">
        </div>
    </div>

</body>
</html>


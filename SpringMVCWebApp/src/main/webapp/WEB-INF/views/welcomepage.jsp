<%-- 
    Document   : welcomepage
    Created on : Aug 11, 2017, 1:48:25 PM
    Author     : kratiyag.gupta
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.List"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<meta http-equiv="Cache-control" content="no-cache">
<meta http-equiv="Cache-control" content="no-store">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="0">
<title>Welcome Page</title>
<spring:url value="/resource/js/jquery-1.11.1.min.js.download"
	var="jquery" />
<spring:url value="/resource/css/bootstrap.min.css" var="bootstrap" />
<spring:url value="/resource/css/welcomepagestyle.css" var="style" />
<script src="${jquery}"></script>
<link href="${bootstrap}" rel="stylesheet" type="text/css" />
<link href="${style}" type="text/css" rel="stylesheet">
<a href="/resource/js/bootstrap.min.js.download"></a>
<spring:url value="/resource/images/1.jpg" var="background" />
</head>
<body style="background-image: url(${background})">
	<!-- Top menu -->
	<nav class="navbar navbar-inverse navbar-no-bg" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#top-navbar-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Intimetec VisonSoft Pvt Ltd.</a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="top-navbar-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a class="scroll-link" href="#">Features</a></li>
					<li><a class="scroll-link" href="#">How it works</a></li>
					<li><a class="scroll-link" href="#">About</a></li>
					<li><a class="scroll-link" href="#">Testimonials</a></li>
					<li><a class="btn btn-link-2" href="logout">Logout</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<h1 align="center" class="color-white">${message}</h1>
	<p></p>
	<div class="table-responsive">
		<h2 align="center" class="color-white">Employees List</h2>
		<table border="2" width="70%" cellpadding="2" align="center"
			class="table table-style table-striped table-hover table-bordered">
			<tr>
				<th>Id</th>
				<th>First Name</th>
				<th>First Name</th>
				<th>Gender</th>
				<th>Date of Birth</th>
				<th>Joining Date</th>
				<th>Email Id</th>
			</tr>
			<c:forEach var="emp" items="${list}">
				<tr>
					<td>${emp.employeeId}</td>
					<td>${emp.firstName}</td>
					<td>${emp.lastName}</td>
					<td>${emp.gender}</td>
					<td>${emp.dateOfBirth}</td>
					<td>${emp.joiningDate}</td>
					<td>${emp.email}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
	<br />
	<a href="newregister">Add New Employee</a>
</body>
</html>

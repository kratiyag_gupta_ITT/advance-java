package com.itt.myapp.configuration;

import com.itt.myapp.controller.HomeController;
import com.itt.myapp.dao.EmployeeDao;
import com.itt.myapp.dao.EmployeeDaoImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import com.itt.myapp.service.EmployeeService;
import com.itt.myapp.service.EmployeeServiceImpl;
import com.itt.myapp.service.ValidatorService;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.itt.myapp")
public class ITTConfiguration extends WebMvcConfigurerAdapter
{
     private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Bean(name = "HelloWorld")
	public ViewResolver viewResolver()
        {
            InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
            viewResolver.setViewClass(JstlView.class);
            viewResolver.setPrefix("/WEB-INF/views/");
            viewResolver.setSuffix(".jsp");
            return viewResolver;
	}
        
    
    
	@Bean(name = "employeeService")
	public EmployeeService employeeService() 
        {
            return new EmployeeServiceImpl();
	}
        
        @Bean
        public DataSource dataSource() 
        {
            DriverManagerDataSource dataSource = new DriverManagerDataSource();
            dataSource.setDriverClassName("com.mysql.jdbc.Driver");
            dataSource.setUrl("jdbc:mysql://localhost:3306/organization");
            dataSource.setUsername("root");
            dataSource.setPassword("kratiyag");
            return dataSource;
        }
        
        @Bean
        public JdbcTemplate jdbcTemplate(DataSource dataSource)
        {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            jdbcTemplate.setResultsMapCaseInsensitive(true);
            return jdbcTemplate;
        }
        
        @Bean(name = "facultyDao")
        public EmployeeDao facultyDao()
        {
            return new EmployeeDaoImpl();
        }
        
        @Bean(name="validatorService")
        public ValidatorService validatorService()
        {
            return new ValidatorService();
        }
        
        @Override  
        public void addResourceHandlers(ResourceHandlerRegistry registry)
        { 
            registry.addResourceHandler("/resource/**").addResourceLocations("/resource/");  
        }  
}
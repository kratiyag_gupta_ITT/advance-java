/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author kratiyag.gupta
 */

public class Employee
	{
		private int employeeId;
		@NotNull(message = "Please enter Date of Birth")
		@NotEmpty(message = "Please enter Date of Birth")
		private String dateOfBirth;
		@NotNull(message = "Please enter Last Name")
		@NotEmpty(message = "Please enter Last Name")
		private String lastName;
		@NotNull(message = "Please enter gender")
		@NotEmpty(message = "Please enter gender")
		private String gender;
		@NotNull(message = "Please enter Joining Date")
		@NotEmpty(message = "Please enter Joining Date")
		private String joiningDate;
		@NotNull(message = "Please enter Email Id")
		@Email(message = "Email Id is not Valid")
		private String email;
		@NotNull(message = "Please enter the valid password should not be empty")
		@Size(min = 7, max = 25, message = "Password should be between 7 to 25 character")
		private String password;
		@NotNull(message = "Please enter First Name")
		@NotEmpty(message = "Please enter First Name")
		private String firstName;
		@NotNull
		private String confirmPassword;

		public String getEmail()
			{
				return email;
			}

		public String getConfirmPassword()
			{
				return confirmPassword;
			}

		public void setConfirmPassword(String confirmPassword)
			{
				this.confirmPassword = confirmPassword;
			}

		public void setEmail(String email)
			{
				this.email = email;
			}

		public String getPassword()
			{
				return password;
			}

		public void setPassword(String password)
			{
				this.password = password;
			}

		public int getEmployeeId()
			{
				return employeeId;
			}

		public void setEmployeeId(int employeeId)
			{
				this.employeeId = employeeId;
			}

		public String getDateOfBirth()
			{
				return dateOfBirth;
			}

		public void setDateOfBirth(String dateOfBirth)
			{
				this.dateOfBirth = dateOfBirth;
			}

		public String getFirstName()
			{
				return firstName;
			}

		public void setFirstName(String firstName)
			{
				this.firstName = firstName;
			}

		public String getLastName()
			{
				return lastName;
			}

		public void setLastName(String lastName)
			{
				this.lastName = lastName;
			}

		public String getGender()
			{
				return gender;
			}

		public void setGender(String gender)
			{
				this.gender = gender;
			}

		public String getJoiningDate()
			{
				return joiningDate;
			}

		public void setJoiningDate(String joiningDate)
			{
				this.joiningDate = joiningDate;
			}
	}

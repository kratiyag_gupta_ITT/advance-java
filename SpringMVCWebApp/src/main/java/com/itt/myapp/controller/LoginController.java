/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.controller;

import com.itt.myapp.domain.Employee;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.itt.myapp.domain.Login;
import com.itt.myapp.service.EmployeeService;
import com.itt.myapp.service.ValidatorService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class LoginController
	{
		private static final Logger logger = LoggerFactory
				.getLogger(HomeController.class);
		@Autowired
		EmployeeService employeeService;
		@Autowired
		ValidatorService validatorService;

		/**
		 * Handles Request for login page for get method
		 * 
		 * @param request
		 * @param response
		 * @return Model and View Object
		 */
		@RequestMapping(value = "/newlogin", method = RequestMethod.GET)
		public ModelAndView showLogin(HttpServletRequest request,
				HttpServletResponse response)
			{
				response.setHeader("Cache-Control","no-cache");
				response.setHeader("Cache-Control","no-store");
				response.setHeader("Pragma","no-cache");
				response.setDateHeader("Expires",0);
				ModelAndView mav = new ModelAndView("newlogin");
				mav.addObject("login",new Login());
				return mav;
			}

		/**
		 * Handles the request for Login and recives the data submitted by the
		 * user
		 * 
		 * @param request
		 * @param response
		 * @param login
		 * @return ModelAndView Object
		 */
		@RequestMapping(value = "/loginProcess", method = RequestMethod.POST)
		public ModelAndView loginProcess(HttpServletRequest request,
				HttpServletResponse response,
				@ModelAttribute("login") Login login)
			{
				response.setHeader("Cache-Control","no-cache");
				response.setHeader("Cache-Control","no-store");
				response.setHeader("Pragma","no-cache");
				response.setDateHeader("Expires",0);
				logger.info("inside the login process");
				ModelAndView mav = null;
				if (!validatorService.isValidEmployeeId(login.getUsername()))
					{
						logger.info("valid employee Id");
						mav = new ModelAndView("newlogin");
						mav.addObject("passwordErrorMessage",
								"Employee Id must only contains Numbers");
						return mav;
					}
				Employee employee = employeeService.validateUser(login);
				if (employee != null)
					{
						logger.info("inside the if statement");
						List<Employee> list = employeeService.getUsersData();
						mav = new ModelAndView("welcomepage");
						mav.addObject("message",
								"Welcome " + employee.getFirstName() + " "
										+ employee.getLastName());
						mav.addObject("list",list);
						request.getSession().setAttribute("employeeId",
								login.getUsername());
						logger.info("after session setup");
					} else
					{
						mav = new ModelAndView("newlogin");
						mav.addObject("passwordErrorMessage",
								"Username or Password is wrong!!");
						mav.addObject("login","login");
					}
				return mav;

			}
	}
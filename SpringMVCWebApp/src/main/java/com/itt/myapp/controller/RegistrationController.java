/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.controller;

import com.itt.myapp.domain.Employee;
import com.itt.myapp.service.EmployeeService;
import com.itt.myapp.service.ValidatorService;
import com.mysql.fabric.Response;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * @author kratiyag.gupta
 */
@Controller
public class RegistrationController
	{
		private static final Logger logger = LoggerFactory
				.getLogger(HomeController.class);
		@Autowired
		public EmployeeService employeeService;
		@Autowired
		public ValidatorService validatorService;

		Employee globalEmployee;

		/**
		 * Calls if request for register page arrives
		 * 
		 * @param request
		 * @param response
		 * @return
		 */
		@RequestMapping(value = "/newregister", method = RequestMethod.GET)
		public ModelAndView newRegister(HttpServletRequest request,
				HttpServletResponse response, ModelMap model)
			{
				ModelAndView mav = new ModelAndView("newregister");
				model.addAttribute("customer",new Employee());
				response.setHeader("Cache-Control","no-cache");
				response.setHeader("Cache-Control","no-store");
				response.setHeader("Pragma","no-cache");
				response.setDateHeader("Expires",0);
				return mav;
			}

		/**
		 * Check for the Session and send the control to welcome page
		 * 
		 * @param request
		 * @param response
		 * @return MOdelAndView Object
		 */
		@RequestMapping(value = "/welcomepage", method = RequestMethod.GET)
		public ModelAndView welcome(HttpServletRequest request,
				HttpServletResponse response)
			{
				ModelAndView mav = null;
				if (request.getSession().getAttribute("employeeId") == null)
					{
						mav = new ModelAndView("newregister");
						return mav;
					}
				response.setHeader("Cache-Control","no-cache");
				response.setHeader("Cache-Control","no-store");
				response.setHeader("Pragma","no-cache");
				response.setDateHeader("Expires",0);
				mav = new ModelAndView("welcomepage");
				return mav;
			}

		/**
		 * Recive the data Entered by the User in the through Model Attribute
		 * check for validation and send request to DAO classes for saving it
		 * 
		 * @param request
		 * @param response
		 * @param employee
		 *            Class holds the data for Employee
		 * @param result
		 * @return
		 */
		@RequestMapping(value = "/registerProcess", method = RequestMethod.POST)
		public ModelAndView registerProcess(HttpServletRequest request,
				HttpServletResponse response,
				@Valid @ModelAttribute("employee") Employee employee,
				BindingResult result)
			{
				response.setHeader("Cache-Control","no-cache");
				response.setHeader("Cache-Control","no-store");
				response.setHeader("Pragma","no-cache");
				response.setDateHeader("Expires",0);
				ModelAndView mav = null;

				if (!result.hasErrors())
					{
						logger.info("no error found");
						if (validatorService
								.isValidBirthDate(employee.getDateOfBirth()))
							{
								if (validatorService
										.isValidEmail(employee.getEmail()))
									{
										if (validatorService.isvalidPawword(
												employee.getPassword(),
												employee.getConfirmPassword()))
											{
												if (employeeService
														.validateUser(employee
																.getEmail()) == null)
													{
														employeeService
																.register(
																		employee);
														List<Employee> list = employeeService
																.getUsersData();
														mav = new ModelAndView(
																"welcomepage");
														mav.addObject("list",
																list);
														mav.addObject("message",
																"Welcome "
																		+ employee
																				.getFirstName()
																		+ " "
																		+ employee
																				.getLastName());
													} else
													{
														mav = new ModelAndView(
																"newregister");
														mav.addObject(
																"emailErrorMessage",
																"A another user already register with above email Id try diffrent one");
													}

											} else
											{
												mav = new ModelAndView(
														"newregister");
												mav.addObject(
														"passwordErrorMessage",
														"Password should be atleast of 8 characters && password and confirm password should be same");
											}

									} else
									{
										mav = new ModelAndView("newregister");
										mav.addObject("emailErrorMessage",
												"Email is not Valid");
									}

							} else
							{
								mav = new ModelAndView("newregister");
								mav.addObject("dobErrorMessage",
										"Invalid date format or date of Birth Employee Should be atleast of 15 years");
								mav.addObject("employee",new Employee());
							}

					} else
					{
						logger.info("error found" + result.getAllErrors());
						mav = new ModelAndView("newregister");
						mav.addObject("employee",employee);
						// mav.addObject("nameErrorMessage", "Invalid Names in
						// First Name or Last Name names must not be null and
						// not contains integer");
					}
				return mav;
			}

	}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author kratiyag.gupta
 */
@Controller
public class LogoutController
	{
		/**
		 * Invalidate the Session When the User press Logout Button
		 * 
		 * @param request
		 * @param response
		 * @return
		 */
		@RequestMapping(value = "/logout", method = RequestMethod.GET)
		public ModelAndView logout(HttpServletRequest request,
				HttpServletResponse response)
			{
				ModelAndView mav = null;
				request.getSession().invalidate();
				mav = new ModelAndView("newlogin");
				return mav;
			}

	}

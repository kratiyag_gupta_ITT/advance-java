package com.springProject.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springProject.SpringDAO.OperationsDao;
import com.springProject.bean.Employee;

@Service("Operations")

public class JDBCOperations implements Operations
	{
		@Autowired
		OperationsDao operationsDao;
		public void addEmployee(Employee employee)
			{
				operationsDao.addEmployee(employee);

			}

		public void deleteEmployee(int EmployeeId)
			{
				operationsDao.deleteEmployee(EmployeeId);
			}

		public void updateEmployeeData(int EmployeeId)
			{
				operationsDao.updateEmployeeData(EmployeeId);

			}

		public List<Employee> printEmployeeData()
			{
				return operationsDao.printEmployeeData();
			}

	}

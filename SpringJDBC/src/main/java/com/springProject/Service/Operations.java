package com.springProject.Service;

import java.util.List;

import com.springProject.bean.Employee;

public interface Operations
	{
		public void addEmployee(Employee employee);
		public void deleteEmployee(int EmployeeId);
		public void updateEmployeeData(int EmployeeId);
		public List<Employee> printEmployeeData();
	}

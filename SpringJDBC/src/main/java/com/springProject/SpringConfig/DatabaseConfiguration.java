package com.springProject.SpringConfig;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@ComponentScan(basePackages = "com.springProject")
@PropertySource(value = { "classpath:application.properties" })
public class DatabaseConfiguration
	{
		@Autowired
		private Environment env;

		@Bean
		public DataSource dataSource()
			{
				/**
				 * 
				 * Creates the object for DriverManagerDataSource
				 * 
				 * Taking the Input from application.properties
				 */
				DriverManagerDataSource dataSource = new DriverManagerDataSource();
				dataSource.setDriverClassName(env.getRequiredProperty("jdbcDriver"));
				dataSource.setUrl(env.getProperty("url"));
				dataSource.setUsername(env.getRequiredProperty("user"));
				dataSource.setPassword(env.getRequiredProperty("password"));
				return dataSource;
			}

		@Bean
		public JdbcTemplate jdbcTemplate(DataSource dataSource)
			{
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				jdbcTemplate.setResultsMapCaseInsensitive(true);
				return jdbcTemplate;
			}
	}

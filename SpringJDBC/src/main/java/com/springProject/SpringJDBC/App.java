package com.springProject.SpringJDBC;

import java.util.List;
import java.util.Scanner;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.springProject.Service.Operations;
import com.springProject.SpringConfig.DatabaseConfiguration;
import com.springProject.bean.Employee;

/**
 *
 * Database operations using Spring JDBC Template
 *
 */
public class App
	{
		private Scanner in;
		private AbstractApplicationContext context;
		private Operations operation;

		/**
		 * Constructor for initializing the context and Scanner Object
		 */
		App()
			{
				context = new AnnotationConfigApplicationContext(
						DatabaseConfiguration.class);
				operation = (Operations) context.getBean("Operations");

				in = new Scanner(System.in);
				int choice;
				do
					{
						/**
						 * Display the Menu for User
						 */
						System.out.println(
								"1.Enter for Entring Data into Employee Table");
						System.out.println(
								"2.Deleting data For a particular Employee");
						System.out.println(
								"3.Retriving data of all the Employees");
						System.out.println("9. For Exiting from the table");
						choice = in.nextInt();
						switch (choice)
							{
							case 1:
								enterData();
								break;
							case 2:
								deleteData();
								break;
							case 3:
								printData();
								break;
							default:
								break;
							}

					} while (choice != 9);
				context.close();
				in.close();
			}

		public static void main(String[] args)
			{
				new App();
			}

		public void enterData()
			{
				Employee employee = new Employee();
				System.out.println(
						"Enter Employee Date of Birth format (YYYY-MM-DD):-");
				in.nextLine();
				employee.setDateOfBirth(in.nextLine());
				System.out.println("Enter the First Name");
				employee.setFirstName(in.nextLine());
				System.out.println("Enter the Last Name");
				employee.setLastName(in.nextLine());
				System.out.println("Enter gender (M/F)");
				employee.setGender(in.nextLine().charAt(0) + "");
				System.out.println(
						"Enter the Joining Date in format (YYYY-MM-DD)");
				employee.setJoiningDate(in.nextLine());
				operation.addEmployee(employee);
			}

		/**
		 * Calls the Dao class through Operation (For loos coupling) for the
		 * Execution of query
		 */
		public void deleteData()
			{
				int employeeId;
				System.out.println(
						"Enter the Id of Employee whose data is to be deleted");
				employeeId = in.nextInt();
				operation.deleteEmployee(employeeId);
			}

		/**
		 * Calls the Dao class
		 */
		public void updateData()
			{
				int employeeId;
				System.out.println(
						"Enter the Id of Employee whose data is to be deleted");
				employeeId = in.nextInt();
				operation.updateEmployeeData(employeeId);
			}

		/**
		 *
		 * call the Dao class which stores the data in the form of List and
		 * print it
		 */
		public void printData()
			{
				List<Employee> employee = operation.printEmployeeData();
				for (Employee e : employee)
					{
						e.printData();
					}
			}

	}

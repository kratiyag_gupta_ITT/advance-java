package com.springProject.SpringDAO;

import java.util.List;
import java.util.Scanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.springProject.bean.Employee;

@Repository
@Qualifier("OperationsDao")
public class JDBCOperationsDao implements OperationsDao
	{
		@Autowired
		JdbcTemplate jdbctemplate;
		
		/**
		 * Add Employee Data to table Using Employee Object getter Setter methods
		 */
		public void addEmployee(Employee employee)
			{
				jdbctemplate.update(
						"Insert into Employee(date_of_Birth,firstName,lastName,gender,joining_date) values(?,?,?,?,?)",
						employee.getDateOfBirth(),employee.getFirstName(),
						employee.getLastName(),employee.getGender(),
						employee.getJoiningDate());
				System.out.println("Data Insertion Sucessful");
			}
		
		/**
		 * Delete the Emmployee Data according to the given Employee Id
		 */
		
		public void deleteEmployee(int EmployeeId)
			{
				jdbctemplate.update("delete from employee where employee_id="+EmployeeId);
				System.out.println("Employee Data Deleted Sucessfully");

			}

		/**
		 * Update the Employee Data
		 */
		public void updateEmployeeData(int EmployeeId)
			{
				String field, value;
				int condition_value;
				Scanner in = new Scanner(System.in);
				System.out.println("Enter the field to be updated");
				field = in.nextLine();
				System.out.println("Enter the field value to be updated");
				value = in.nextLine();
				System.out.println("Enter the Employee id");
				condition_value = in.nextInt();
				jdbctemplate.update("Update table employee set " + field + "='"
						+ value + "' where employee_Id=" + condition_value);
				System.out.println();
				in.close();
			}
		/**
		 * Select qury Executed and  each row is store in the form of object in list
		 */

		public List<Employee> printEmployeeData()
			{
				List <Employee> persons = jdbctemplate.query("SELECT * FROM employee", new BeanPropertyRowMapper<Employee>(Employee.class));
		        return persons;
			}

	}

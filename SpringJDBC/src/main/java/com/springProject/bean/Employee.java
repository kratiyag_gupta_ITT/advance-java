package com.springProject.bean;

public class Employee
	{
		private int employeeId;
		private String dateOfBirth,firstName,lastName,gender,joiningDate;
		public String getDateOfBirth()
			{
				return dateOfBirth;
			}
		public void setDateOfBirth(String dateOfBirth)
			{
				this.dateOfBirth = dateOfBirth;
			}
		public int getEmployeeId()
			{
				return employeeId;
			}
		public void setEmployeeId(int employeeId)
			{
				this.employeeId = employeeId;
			}
				public String getFirstName()
			{
				return firstName;
			}
		public void setFirstName(String firstName)
			{
				this.firstName = firstName;
			}
		public String getLastName()
			{
				return lastName;
			}
		public void setLastName(String lastName)
			{
				this.lastName = lastName;
			}
		public String getGender()
			{
				return gender;
			}
		public void setGender(String gender)
			{
				this.gender = gender;
			}
		public String getJoiningDate()
			{
				return joiningDate;
			}
		public void setJoiningDate(String joiningDate)
			{
				this.joiningDate = joiningDate;
			}
		/**
		 * For printing the Employee Data
		 */
		public void printData()
		{
			System.out.println("Employee id : "+getEmployeeId()+" Employee name: "+getFirstName()+" "+getLastName()+"\n");
			System.out.println("Gender : "+ getGender()+" Employee Joining data: "+getJoiningDate()+"\n");
		}
	}

package com.websystique.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
@Configuration
//@ComponentScan("com.websystique.spring")
public class AppConfig {
	/**
	 * 
	 * @return		Object of DemoLifeCycle
	 */
	@Bean
	public LifeCycle getLifeObj() 
		{
			return new LifeCycle();
		}
	
	/**
	 * 
	 * @return		object of DemoPostProcessor
	 */
	@Bean
	public DemoPostProcessor getPostObj() {
		return new DemoPostProcessor();
	}
 
}

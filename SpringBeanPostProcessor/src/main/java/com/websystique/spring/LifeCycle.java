package com.websystique.spring;

import org.springframework.stereotype.Component;
import javax.annotation.*;

@Component
public class LifeCycle {
	
	public void greetings()
	{
		System.out.println("Welcome to bean life cycle");
	}
	@SuppressWarnings("restriction")
	@PostConstruct
	public void init()
	{
		System.out.println("I m Currently in INIT method");
	}
	@SuppressWarnings("restriction")
	@PreDestroy
	public void  destroy()
	{
		System.out.println("I m currently in destroy method");
		
	}

}

package com.websystique.spring;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.websystique.spring.AppConfig;

public class AppTest
	{

		public static void main(String[] args)
			{

				AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
				ctx.register(AppConfig.class); //register the bean object
				ctx.refresh();
				LifeCycle service = ctx.getBean(LifeCycle.class);
				service.greetings();
				DemoPostProcessor post = ctx.getBean(DemoPostProcessor.class);
				ctx.registerShutdownHook();
				ctx.destroy();
				ctx.close();

			}

	}

package com.example.Main;


import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

public class SpringExpressionLanguage
	{

		public static void main(String[] args) throws Exception
			{
				/**
				 * Using In build Concat Method
				 */
				ExpressionParser parser = new SpelExpressionParser();
				String message = (String) parser
						.parseExpression(
								"'Good Morning Every one'.concat('!!!!!')") //adds the string !!!! after message
						.getValue();
				System.out.println(message);
				Calculation calculation=new Calculation();  
				StandardEvaluationContext context=new StandardEvaluationContext(calculation);  
				parser = new SpelExpressionParser();
				/**
				 * Assigning value to Calculation class variable number
				 */
				parser.parseExpression("number").setValue(context,"5");  
				System.out.println(calculation.cube());  
			}

	}
